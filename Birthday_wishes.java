import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import java.util.Properties;    
import javax.mail.*;    
import javax.mail.internet.*;    
class SendEmail{  
	
	/*
	 * Method to send the email
	 * Takes sender address, sender account password, recipient address, subject and message as parameters
	 */
	
    public static void send(String from,String password,String to,String sub,String msg){  
    	/*
         * Get properties object and set keys and values
         * Setting the host, port and authentication   
         */    
          Properties props = new Properties();    
          props.put("mail.smtp.host", "smtp.gmail.com");    
          props.put("mail.smtp.socketFactory.port", "465");    
          props.put("mail.smtp.socketFactory.class",    
                    "javax.net.ssl.SSLSocketFactory");    
          props.put("mail.smtp.auth", "true");    
          props.put("mail.smtp.port", "465");    
          /*
           * Getting the session object before composing the message.
           * Authenticates the sender and returns the default session  
           */  
          Session session = Session.getDefaultInstance(props,    
           new javax.mail.Authenticator() {    
           protected PasswordAuthentication getPasswordAuthentication() {    
           return new PasswordAuthentication(from,password);  
           }    
          });
          //compose message    
          try {    
           MimeMessage message = new MimeMessage(session); 
           //adding the recipient
           message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));
           //setting the subject to the mail
           message.setSubject(sub);
           //setting message in the body of the email
           message.setText(msg);    
           //send Email  
           Transport.send(message);    
           System.out.println("message sent successfully");    
          } catch (MessagingException e) {throw new RuntimeException(e);}    
             
    }  
}  
public class Birthday_wishes {
	//Declaring the Common message to all
	private String message= "All of us at Talent Sculptors, Wish you a very Happy Birthday.\n\nStay Blessed. \nBest Regards,\nTalent Sculptors extended family.";
    private String inputFile;
    private Cell mailCell;
    private Cell nameCell;
    private Cell dateCell;
  //setting the input excel file
    public void setInputFile(String inputFile) {
        this.inputFile = inputFile;
    }

    public void read() throws IOException  {
        File inputWorkbook = new File(inputFile);
        Workbook work;
        try {
            work = Workbook.getWorkbook(inputWorkbook);
            
            Sheet sheet = work.getSheet(0);
          //Get the current date from the system
            String date = new SimpleDateFormat("MM.dd").format(new java.util.Date());
            System.out.println(date);
            /*
             * loop through the excel sheet rows and get the recipient address and name
             * if date of birth matches with the current date
             */
            for (int i = 1; i < sheet.getRows(); i++) {
            		dateCell = sheet.getCell(1, i);
            		if(date.equals(dateCell.getContents())) {
            			nameCell = sheet.getCell(0,i);
                        System.out.println(nameCell.getContents());
                        
                        mailCell = sheet.getCell(3, i);
                        System.out.println(mailCell.getContents());
                      //call the send method to send the email
                        SendEmail.send("adithya.642@gmail.com", "7382225938@", mailCell.getContents(), "Birthday wishes", "Dear "+nameCell.getContents()+",\n"+message);
            		}           
            }
           
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException  {
    	//creating a Birthday_wishes object
        Birthday_wishes test = new Birthday_wishes();
        test.setInputFile("D:\\adithya folder\\BDwishes.xls");
        test.read();
    }

}